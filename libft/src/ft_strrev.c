/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 23:10:07 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:54:15 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <libft.h>

char		*ft_strrev(char *s)
{
	size_t	len;
	size_t	i;
	char	c;

	len = ft_strlen(s);
	i = 0;
	while (i < len / 2)
	{
		c = s[i];
		s[i] = s[len - 1 - i];
		s[len - 1 - i++] = c;
	}
	return (s);
}
