/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 07:19:28 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:51:17 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void				*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*ptr;
	unsigned char	ch;
	size_t			i;

	i = -1;
	ptr = (unsigned char *)s;
	ch = (unsigned char)c;
	while (++i < n)
	{
		if (ptr[i] == ch)
			return ((void*)&ptr[i]);
	}
	return (NULL);
}
