/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 00:14:23 by fmontaro          #+#    #+#             */
/*   Updated: 2014/07/18 16:46:22 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_strnlen(const char *str, size_t n)
{
	size_t		i;

	i = 0;
	while (str[i] && i < n)
		i++;
	return (i);
}

size_t			ft_strlcat(char *dest, const char *src, size_t n)
{
	size_t		s_len;
	size_t		d_len;

	s_len = ft_strlen(src);
	d_len = ft_strlen(dest);
	if (n > d_len + 1)
		ft_strncat(dest, src, n - d_len - 1);
	else
		return (ft_strnlen(dest, n) + s_len);
	return (d_len + s_len);
}
