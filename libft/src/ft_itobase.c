/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itobase.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 23:10:07 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:54:15 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_itobase(int n, char *base, size_t base_len)
{
	size_t		cnt;
	size_t		div;
	size_t		tmp;
	size_t		pdiv;
	char		*str;

	cnt = 0;
	div = 1;
	while (n / div > base_len)
		div *= base_len;
	pdiv = div;
	while (pdiv && ++cnt)
		pdiv /= base_len;
	str = ft_memalloc(cnt + 1);
	pdiv = 0;
	while (div)
	{
		tmp = n / div % base_len;
		if (tmp > base_len - 1)
			str[pdiv++] = base[tmp - 10];
		else
			str[pdiv++] = base[tmp];
		div /= base_len;
	}
	return (str);
}
