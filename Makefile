# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/06 17:08:33 by fmontaro          #+#    #+#              #
#    Updated: 2015/01/09 14:20:12 by fmontaro         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		= bistro

SHELL		= /bin/zsh
CC			= clang
INCS		= -Iinc -Ilibft/inc
CFLAGS		= -Ofast -ffast-math -g0 -march=native -mtune=native
CFLAGS		+= -W -Werror -Wextra -Wall
LIBS		= -L./libft -lft

FILES		= add.c				\
			  args.c			\
			  bigint.c			\
			  bigint_util.c		\
			  div.c				\
			  elem_util.c		\
			  error.c			\
			  eval.c			\
			  fac.c				\
			  format.c			\
			  function.c		\
			  info.c			\
			  kara.c			\
			  main.c			\
			  max_min.c			\
			  mod.c				\
			  mul.c				\
			  mv_list.c			\
			  num.c				\
			  opt.c				\
			  output.c			\
			  parser.c			\
			  pow.c				\
			  sep.c				\
			  shuting_yard.c	\
			  sub.c				\
			  toom3.c			\
			  util.c

SRC			= $(addprefix src/, $(FILES))
OBJ			= $(SRC:src/%.c=obj/%.o)

.SILENT: all $(NAME) clean fclean re dirobj clibft toto

all: dirobj clibft $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LIBS)
	echo "\033[33;3m$(NAME) \033[32;2m\t\t\tCreated\033[0m"

obj/%.o: src/%.c
	@$(CC) $(INCS) $(CFLAGS) -o $@ -c $^

clean:
	make clean -C libft --no-print-directory
	test ! -d obj || \
		echo "\033[31;4mDelete:\033[0m\033[32;2m\t\t\tobj/\033[0m" && \
		rm -rf obj

fclean: clean
	make fclean -C libft --no-print-directory
	test ! -f $(NAME) || \
		echo "\033[31;4mDelete:\033[0m\033[32;2m\t\t\t$(NAME)\033[0m" && \
		rm -f $(NAME)

re: fclean all

clibft:
	make re -C libft --no-print-directory

dirobj:
	test -d obj || \
		echo "\033[33;4mCreate:\033[0m\033[32;2m\t\t\tobj/\033[0m" && \
		mkdir -p obj

test:
	@echo -n "\033[0m"
	@echo "tests:"
	@echo "------------------------------------------------------------------------------\n"
	./$(NAME) "-(2^3%4/5-6*7)"
	@echo "------------------------------------------------------------------------------\n"
	./$(NAME) "(9999^12/(4*652) - 2^14*(2+99999999999999999)*2^77)/(10002245877*12220024^4*4250)-100"
	@echo "------------------------------------------------------------------------------\n"
	@printf "\033[33m"
	time ./$(NAME) "01234" "-+*/%^!#&" "()" "44444^44444" > a
	@printf "\033[35m"
	time echo "obase=5;ibase=5;44444^44444" | bc > b
	@echo -n "\033[0m"
	diff a b
	@./test.sh a b
	@echo "------------------------------------------------------------------------------\n"
	@printf "\033[33m"
	time ./$(NAME) "999^999" > a
	@printf "\033[35m"
	time echo "999^999" | bc > b
	@echo -n "\033[0m"
	diff a b
	@./test.sh a b
	@echo "------------------------------------------------------------------------------\n"
	@printf "\033[33m"
	time ./$(NAME) "9999^9999" > a
	@printf "\033[35m"
	time echo "9999^9999" | bc > b
	@echo -n "\033[0m"
	diff a b
	@./test.sh a b
	@echo "------------------------------------------------------------------------------\n"
	@printf "\033[33m"
	time ./$(NAME) "99999^99999" > a
	@printf "\033[35m"
	time echo "99999^99999" | bc > b
	@echo -n "\033[0m"
	diff a b
	@./test.sh a b
	@rm -f a b

