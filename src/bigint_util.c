/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bigint_util.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:06:17 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/08 15:19:55 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

char					is_odd(t_bigint *bi)
{
	long				i;
	long				count;

	if (g_info.base_len % 2 == 0)
		return (*bi->val % 2 != 0);
	i = 0;
	count = 0;
	while (i < bi->len)
	{
		if (bi->val[i++] % 2)
			++count;
	}
	return (count % 2);
}

t_elem					*inv(t_elem *n)
{
	n->bi->sign = n->bi->sign ? 0 : 1;
	return (n);
}

char					cmp(t_bigint *a, t_bigint *b)
{
	long				i;

	if (a->sign != b->sign)
		return (a->sign ? -1 : 1);
	if (a->len < b->len)
		return (a->sign ? 1 : -1);
	if (a->len > b->len)
		return (a->sign ? -1 : 1);
	i = 0;
	while (i < a->len)
	{
		if (a->val[a->len - i - 1] < b->val[a->len - i - 1])
			return (a->sign ? 1 : -1);
		if (a->val[a->len - i - 1] > b->val[a->len - i - 1])
			return (a->sign ? -1 : 1);
		++i;
	}
	return (0);
}

t_elem					*cpy(t_elem *n)
{
	t_bigint			*cpy;
	long				i;

	cpy = new_bigint(n->bi->len);
	i = 0;
	while (i < n->bi->len)
	{
		cpy->val[i] = n->bi->val[i];
		++i;
	}
	cpy->sign = n->bi->sign;
	return (new_num(cpy));
}

t_bigint				*clean(t_bigint *bi)
{
	long				i;

	i = bi->len - 1;
	while (i >= 0 && bi->val[i] == 0)
	{
		--i;
		bi->len--;
	}
	return (bi);
}
