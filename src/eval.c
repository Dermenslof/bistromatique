/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:06:55 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:06:58 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info		g_info;

static t_elem		*calculate(t_elem *a, char opt, t_elem *b)
{
	int				i;
	char			fac;

	fac = opt == g_info.opt[6];
	if ((!a && !fac) || !b || (!fac && a->type != NUM) || b->type != NUM)
	{
		free_elem(&a);
		free_elem(&b);
		print_error(2);
	}
	i = -1;
	while (g_info.func[++i].opt)
	{
		if (opt == g_info.func[i].opt)
			return (g_info.func[i].f(b, a));
	}
	print_error(2);
	return (NULL);
}

void				eval(t_elem **stack)
{
	t_elem			*buf;
	t_elem			*tmp;
	t_elem			*a;

	buf = NULL;
	while (*stack)
	{
		tmp = pop(stack);
		if (tmp->type == OPT)
		{
			if (tmp->c == g_info.opt[6])
				push(stack, calculate(NULL, tmp->c, pop(&buf)));
			else
			{
				a = pop(&buf);
				push(stack, calculate(a, tmp->c, pop(&buf)));
			}
			free_elem(&tmp);
			if (!(*stack)->next)
				break ;
		}
		else
			push(&buf, tmp);
	}
	print_result(stack);
}
