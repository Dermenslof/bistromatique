/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   args.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:06:09 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:06:11 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

static char				contains(char *tab, char c)
{
	int					i;

	i = -1;
	while (tab[++i])
	{
		if (tab[i] == c)
			return (1);
	}
	return (0);
}

static void				check_base(void)
{
	int					i;
	int					j;
	char				*tab;

	if (g_info.base_len < 2)
		print_error(1);
	if (!(tab = (char *)ft_memalloc(g_info.base_len)))
		print_error(3);
	i = -1;
	while (g_info.base[++i])
	{
		j = -1;
		while (++j < i)
		{
			if (ft_isspace(tab[i]) || tab[j] == g_info.base[i])
			{
				free(tab);
				print_error(1);
			}
		}
		tab[i] = g_info.base[i];
	}
	free(tab);
}

static void				check_opt(void)
{
	int					i;
	int					j;
	char				*tab;

	if (ft_strlen(g_info.opt) != 9)
		print_error(1);
	if (!(tab = (char *)ft_memalloc(9)))
		print_error(3);
	i = -1;
	while (++i < 9)
	{
		j = -1;
		while (++j < i)
		{
			if (ft_isspace(tab[i]) || tab[j] == g_info.opt[i]
					|| contains(g_info.base, tab[j]))
			{
				free(tab);
				print_error(1);
			}
		}
		tab[i] = g_info.opt[i];
	}
	free(tab);
}

static void				check_sep(void)
{
	int					i;
	int					j;
	char				*tab;

	if (ft_strlen(g_info.sep) != 2)
		print_error(1);
	if (!(tab = (char *)ft_memalloc(2)))
		print_error(3);
	i = -1;
	while (++i < 2)
	{
		j = -1;
		while (++j < i)
		{
			if (ft_isspace(tab[i]) || tab[j] == g_info.sep[i]
					|| contains(g_info.base, tab[j])
					|| contains(g_info.opt, tab[j]))
			{
				free(tab);
				print_error(1);
			}
		}
		tab[i] = g_info.sep[i];
	}
	free(tab);
}

void					check_args(void)
{
	check_base();
	check_opt();
	check_sep();
}
