/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   kara.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:14 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/09 15:24:07 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info	g_info;

static void		init_kara(t_kara *k, t_ulong **num, t_ulong *ret, long d)
{
	k->r[0] = &num[0][0];
	k->l[0] = &num[0][d / 2];
	k->r[1] = &num[1][0];
	k->l[1] = &num[1][d / 2];
	k->sum[0] = &ret[d * 5];
	k->sum[1] = &ret[d * 5 + d / 2];
	k->x1 = &ret[d * 0];
	k->x2 = &ret[d * 1];
	k->x3 = &ret[d * 2];
}

static void		simple_kara(t_ulong *a, t_ulong *b, t_ulong *ret, long d)
{
	long			i;
	long			j;

	i = 0;
	while (i < 2 * d)
		ret[i++] = 0;
	i = 0;
	while (i < d)
	{
		j = 0;
		while (j < d)
		{
			ret[i + j] += a[i] * b[j];
			j++;
		}
		++i;
	}
}

static void		kara(t_ulong **num, t_ulong *ret, long d)
{
	long		i;
	t_kara		s;

	init_kara(&s, num, ret, d);
	if (d <= KARA_CUTOFF)
	{
		simple_kara(num[0], num[1], ret, d);
		return ;
	}
	i = 0;
	while (i++ < d / 2)
	{
		s.sum[0][i - 1] = s.l[0][i - 1] + s.r[0][i - 1];
		s.sum[1][i - 1] = s.l[1][i - 1] + s.r[1][i - 1];
	}
	kara(s.r, s.x1, d / 2);
	kara(s.l, s.x2, d / 2);
	kara(s.sum, s.x3, d / 2);
	i = 0;
	while (i++ < d)
		s.x3[i - 1] = s.x3[i - 1] - s.x1[i - 1] - s.x2[i - 1];
	i = 0;
	while (i++ < d)
		ret[i + d / 2 - 1] += s.x3[i - 1];
}

static void		carry(t_bigint *bi, long d)
{
	t_ulong		c;
	long		i;

	c = 0;
	i = 0;
	while (i < d)
	{
		bi->val[i] += c;
		if (bi->val[i] > UINT32_MAX - 11)
			c = -(-(bi->val[i] + 1) / g_info.base_len + 1);
		else
			c = bi->val[i] / g_info.base_len;
		bi->val[i] -= c * g_info.base_len;
		if (bi->val[i++])
			bi->len = i;
	}
}

t_elem			*pre_kara(t_elem *n1, t_elem *n2)
{
	t_ulong		*num[2];
	t_bigint	*r;
	long		d;
	long		i;

	i = n1->bi->len;
	d = 1;
	while (d < i)
		d *= 2;
	scale(&num[0], n1->bi, d);
	scale(&num[1], n2->bi, d);
	free_elem(&n1);
	free_elem(&n2);
	r = new_bigint(6 * d);
	kara(num, r->val, d);
	carry(r, 2 * d);
	free(num[0]);
	free(num[1]);
	return (new_num(r));
}
