/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   function.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 14:13:43 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/08 14:29:00 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

void			init_functions(t_f **f)
{
	f[0] = pre_sub;
	f[1] = pre_add;
	f[2] = pre_mul;
	f[3] = pre_div;
	f[4] = pre_mod;
	f[5] = pre_pow;
	f[6] = pre_fac;
	f[7] = pre_max;
	f[8] = pre_min;
}
