/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mul.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:33 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/09 15:24:40 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info	g_info;

static void		carry(t_bigint *r)
{
	t_ulong		carry;
	long		i;
	long		len;

	len = r->len;
	r->len = 0;
	carry = 0;
	i = 0;
	while (i < len)
	{
		r->val[i] += carry;
		carry = r->val[i] / g_info.base_len;
		r->val[i] %= g_info.base_len;
		if (r->val[i])
			r->len = i + 1;
		++i;
	}
}

void			simple_mul(t_bigint *r, t_bigint *a, t_bigint *b)
{
	long		i;
	long		j;

	j = 0;
	while (j < b->len)
	{
		i = 0;
		while (i < a->len)
		{
			r->val[i + j] += a->val[i] * b->val[j];
			++i;
		}
		++j;
	}
}

t_elem			*pre_mul(t_elem *a, t_elem *b)
{
	t_bigint	*r;

	if (a->bi->sign && b->bi->sign)
		return (pre_mul(inv(a), inv(b)));
	if (a->bi->sign)
		return (inv(pre_mul(inv(a), b)));
	if (b->bi->sign)
		return (inv(pre_mul(a, inv(b))));
	if (a->bi->len < b->bi->len)
		return (pre_mul(b, a));
	if (b->bi->len > 10)
		return (pre_kara(a, b));
	r = new_bigint(a->bi->len + b->bi->len);
	simple_mul(r, a->bi, b->bi);
	carry(r);
	free_elem(&a);
	free_elem(&b);
	return (new_num(r));
}
