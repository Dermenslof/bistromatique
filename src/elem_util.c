/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elem_util.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:06:27 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:06:30 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info		g_info;

char				free_elem(t_elem **elem)
{
	if (*elem)
	{
		if ((*elem)->type == NUM)
		{
			free((*elem)->bi->val);
			free((*elem)->bi);
		}
		free(*elem);
		*elem = NULL;
	}
	return (1);
}
