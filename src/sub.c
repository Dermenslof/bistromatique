/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sub.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:08:21 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:08:24 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

static void				carry(t_bigint *r)
{
	t_ulong				carry;
	long				i;
	long				len;

	len = r->len;
	r->len = 0;
	carry = 0;
	i = 0;
	while (i < len)
	{
		r->val[i] -= carry;
		carry = 0;
		if (r->val[i] > UINT32_MAX - 11 && (carry = 1))
			r->val[i] += g_info.base_len;
		else
			r->val[i] %= g_info.base_len;
		if (r->val[i])
			r->len = i + 1;
		++i;
	}
}

static void				sub(t_bigint *r, const t_bigint *a, const t_bigint *b)
{
	long				i;

	i = 0;
	while (i < a->len + 1)
	{
		if (i < a->len)
			r->val[i] = i < b->len ? a->val[i] - b->val[i] : a->val[i];
		++i;
	}
}

t_elem					*pre_sub(t_elem *a, t_elem *b)
{
	t_bigint			*r;

	if (a->bi->sign && b->bi->sign)
		return (pre_sub(inv(b), inv(a)));
	if (a->bi->sign)
		return (inv(pre_add(inv(a), b)));
	if (b->bi->sign)
		return (pre_add(a, inv(b)));
	if (cmp(a->bi, b->bi) < 0)
		return (inv(pre_sub(b, a)));
	r = new_bigint(a->bi->len);
	sub(r, a->bi, b->bi);
	carry(r);
	free_elem(&a);
	free_elem(&b);
	return (new_num(r));
}
