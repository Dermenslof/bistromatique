/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   max_min.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:24 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:07:25 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info	g_info;

t_elem			*pre_max(t_elem *a, t_elem *b)
{
	if (cmp(a->bi, b->bi) < 0)
	{
		free_elem(&a);
		return (b);
	}
	free_elem(&b);
	return (a);
}

t_elem			*pre_min(t_elem *a, t_elem *b)
{
	if (cmp(a->bi, b->bi) < 0)
	{
		free_elem(&b);
		return (a);
	}
	free_elem(&a);
	return (b);
}
