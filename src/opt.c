/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   opt.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:53 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:35:55 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info		g_info;

char				get_priority(char c)
{
	static char		tab[] = {1, 1, 2, 2, 2, 0, 0, 4, 4};
	char			*opt;
	int				i;

	opt = g_info.opt;
	i = -1;
	while (++i < 9)
	{
		if (*(opt++) == c)
			return (tab[i]);
	}
	return (-1);
}

char				is_opt(char c)
{
	char			*opt;

	opt = g_info.opt;
	while (*opt)
	{
		if (*(opt++) == c)
			return (1);
	}
	return (0);
}

void				set_opt(t_elem **expr, t_elem *elem)
{
	t_elem			*prev;
	t_elem			*next;

	prev = elem->prev;
	next = elem->next;
	if (prev && prev->type != NUM)
	{
		if (prev->c == *g_info.sep && elem->c != g_info.opt[6])
			print_error(2);
		if (get_priority(elem->c) == 1)
		{
			if (get_priority(prev->c) == 1)
			{
				elem->c = elem->c != prev->c ? *g_info.opt : g_info.opt[1];
				pop_middle(expr, elem);
				set_opt(expr, elem);
			}
			else if (next && next->type != NUM && next->c != *g_info.sep)
				print_error(2);
		}
		else if (prev->c != g_info.sep[1] && elem->c != g_info.opt[6])
			print_error(2);
	}
	if (!next || (!prev && elem->c != g_info.opt[6]))
		print_error(2);
}

t_elem				*new_opt(char c)
{
	t_elem			*elem;

	if (!(elem = (t_elem *)malloc(sizeof(t_elem))))
		print_error(3);
	elem->type = OPT;
	elem->c = c;
	elem->do_pop = !get_priority(c);
	elem->prev = NULL;
	elem->next = NULL;
	return (elem);
}
