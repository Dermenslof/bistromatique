/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fac.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:00 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:07:03 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info		g_info;

static char			free_elems(t_elem *a, t_elem *z, t_elem *o)
{
	free_elem(&a);
	free_elem(&z);
	free_elem(&o);
	return (1);
}

static t_elem		*fac(t_elem *a, t_elem *one)
{
	t_elem			*tmp;

	tmp = pre_sub(cpy(a), cpy(one));
	while (cmp(tmp->bi, one->bi))
	{
		a = pre_mul(a, cpy(tmp));
		tmp = pre_sub(tmp, cpy(one));
	}
	free_elems(one, tmp, NULL);
	return (a);
}

t_elem				*pre_fac(t_elem *a, t_elem *unused)
{
	t_elem			*one;
	t_elem			*zero;

	(void)unused;
	if (a->bi->sign)
		return (inv(pre_fac(inv(a), NULL)));
	one = new_num(l_to_bi(1));
	zero = new_num(l_to_bi(0));
	if ((!cmp(a->bi, one->bi) || !cmp(a->bi, zero->bi))
			&& free_elems(a, zero, NULL))
		return (one);
	free_elem(&zero);
	return (fac(a, one));
}
