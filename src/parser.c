/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:08:02 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:08:04 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info		g_info;

static void			organize(t_elem **expr)
{
	t_elem			*tmp;
	int				grp;

	grp = 0;
	tmp = *expr;
	while (tmp->next)
		tmp = tmp->next;
	while (tmp)
	{
		if (tmp->type == SEP)
		{
			set_sep(tmp);
			grp += tmp->c == *g_info.sep ? -1 : 1;
		}
		else if (tmp->type == OPT)
			set_opt(expr, tmp);
		else
			set_nb(expr, tmp);
		tmp = tmp->prev;
	}
	if (grp)
		print_error(2);
}

void				parse(t_elem **stack, char *str)
{
	char			*s;
	t_elem			*expr;

	expr = NULL;
	g_info.tmp = expr;
	while (*str)
	{
		while (ft_isspace(*str))
			str++;
		if (is_sep(*str))
			push_end(&expr, new_sep(*str++));
		else if (is_opt(*str))
			push_end(&expr, new_opt(*str++));
		else if (!is_num(*str))
			print_error(2);
		if (is_num(*str))
		{
			s = str;
			while (is_num(*str))
				str++;
			push_end(&expr, new_nb(ft_strsub(s, 0, str - s)));
		}
	}
	organize(&expr);
	shuting_yard(stack, &expr);
}
