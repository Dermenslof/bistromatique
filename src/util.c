/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:08:26 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/09 15:18:21 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info		g_info;

char				*join(char *a, char *b)
{
	char			*tmp;

	tmp = ft_strjoin(a, b);
	free(a);
	free(b);
	return (tmp);
}

t_elem					*cpy_l(t_ulong *n, long d)
{
	t_bigint			*cpy;
	long				i;

	cpy = new_bigint(d);
	i = -1;
	while (++i < d)
		cpy->val[i] = n[i];
	return (new_num(cpy));
}
