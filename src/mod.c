/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mod.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:27 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:07:31 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info	g_info;

static t_elem			*mod(t_elem *a, t_elem *b)
{
	return (pre_sub(a, pre_mul(b, pre_div(cpy(a), cpy(b)))));
}

t_elem					*pre_mod(t_elem *a, t_elem *b)
{
	char				diff;

	if ((diff = cmp(a->bi, b->bi)) < 1)
	{
		free_elem(&b);
		if (diff)
			return (a);
		free_elem(&a);
		return (new_num(l_to_bi(0)));
	}
	return (mod(a, b));
}
