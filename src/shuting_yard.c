/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shuting_yard.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:08:15 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:08:17 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

static void				process_opt(t_elem **stack, t_elem **opt, t_elem *tmp)
{
	char				priority;

	if (*opt)
	{
		priority = get_priority(tmp->c);
		if (get_priority((*opt)->c) > priority && priority)
		{
			while (*opt && (*opt)->type != SEP)
				push_end(stack, pop(opt));
		}
		else if (get_priority((*opt)->c) == priority)
			push_end(stack, pop(opt));
	}
	push(opt, tmp);
}

static void				process_sep(t_elem **stack, t_elem **opt, t_elem *tmp)
{
	t_elem				*del;

	if (tmp->c == g_info.sep[1])
	{
		while (*opt && (*opt)->type != SEP)
			push_end(stack, pop(opt));
		del = pop(opt);
		free_elem(&del);
		free_elem(&tmp);
		return ;
	}
	push(opt, tmp);
}

void					shuting_yard(t_elem **stack, t_elem **expr)
{
	t_elem				*opt_stack;
	t_elem				*tmp;

	opt_stack = NULL;
	g_info.tmp2 = opt_stack;
	while ((tmp = pop(expr)))
	{
		if (tmp->type == OPT)
			process_opt(stack, &opt_stack, tmp);
		else if (tmp->type == SEP)
			process_sep(stack, &opt_stack, tmp);
		else
		{
			push_end(stack, tmp);
			if (opt_stack && opt_stack->do_pop)
				push_end(stack, pop(&opt_stack));
		}
	}
	while (opt_stack)
		push_end(stack, pop(&opt_stack));
}
