/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sep.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:08:11 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:36:47 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

char				is_sep(char c)
{
	char			*sep;

	sep = g_info.sep;
	while (*sep)
	{
		if (*(sep++) == c)
			return (1);
	}
	return (0);
}

void				set_sep(t_elem *elem)
{
	t_elem			*prev;

	prev = elem->prev;
	if (!prev && elem->c == g_info.sep[1])
		print_error(2);
	else if (elem->c == *g_info.sep && prev && prev->type == NUM)
		print_error(2);
	else if (elem->c != *g_info.sep && prev && prev->type == OPT)
		print_error(2);
}

t_elem				*new_sep(char c)
{
	t_elem			*elem;

	if (!(elem = (t_elem *)malloc(sizeof(t_elem))))
		print_error(3);
	elem->type = SEP;
	elem->c = c;
	elem->prev = NULL;
	elem->next = NULL;
	return (elem);
}
