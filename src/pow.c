/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pow.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:08:06 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:36:30 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info	g_info;

static char		free_elems(t_elem *a, t_elem *b, t_bigint *c, t_elem *d)
{
	free_elem(&a);
	free_elem(&b);
	if (c)
		free(c->val);
	free(c);
	free_elem(&d);
	return (1);
}

static void		my_pow(t_elem **r, t_elem *a, t_elem *b)
{
	while (b->bi->len)
	{
		if (is_odd(b->bi))
			*r = pre_mul(*r, cpy(a));
		a = pre_mul(a, cpy(a));
		b = div_by_two(b);
	}
	free_elems(a, b, NULL, NULL);
}

t_elem			*pre_pow(t_elem *a, t_elem *b)
{
	t_bigint	*one;
	t_elem		*r;

	if (a->bi->sign)
		return (inv(pre_pow(inv(a), b)));
	if (b->bi->sign && free_elems(a, b, NULL, NULL))
		return (new_num(l_to_bi(0)));
	one = l_to_bi(1);
	if (!a->bi->len && free_elems(a, b, one, NULL))
		return (new_num(l_to_bi(0)));
	if ((!b->bi->len || !cmp(a->bi, one)) && free_elems(a, b, NULL, NULL))
		return (new_num(one));
	r = new_num(l_to_bi(1));
	my_pow(&r, a, b);
	free_elems(NULL, NULL, one, NULL);
	return (r);
}
