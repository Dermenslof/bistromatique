/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   num.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:47 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/09 15:18:20 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

char					is_num(char c)
{
	char				*num;

	num = g_info.base;
	while (*num)
	{
		if (*(num++) == c)
			return (1);
	}
	return (0);
}

void					set_nb(t_elem **expr, t_elem *elem)
{
	t_elem				*prev;

	if (elem->prev)
	{
		prev = elem->prev->prev;
		if (elem->prev->type == NUM && prev
				&& prev->type == OPT && get_priority(prev->c) == 4)
			return ;
		if (elem->prev->type == NUM || elem->prev->c == g_info.sep[1])
			print_error(2);
		if (elem->prev->c == *g_info.opt)
		{
			if (prev && prev->type == OPT && prev->c == *g_info.opt)
			{
				elem->prev->c = g_info.opt[1];
				pop_middle(expr, elem->prev);
			}
			else if (!prev || prev->type == OPT || prev->c == *g_info.sep)
			{
				elem->bi->sign = 1;
				pop_middle(expr, elem);
			}
		}
	}
}

t_elem					*new_num(t_bigint *bi)
{
	t_elem				*tmp;

	if (!(tmp = (t_elem *)malloc(sizeof(t_elem))))
		print_error(3);
	tmp->bi = bi;
	tmp->type = NUM;
	tmp->prev = NULL;
	tmp->next = NULL;
	return (tmp);
}

t_elem					*new_nb(char *n)
{
	t_elem				*elem;

	if (!(elem = (t_elem *)malloc(sizeof(t_elem))))
		print_error(3);
	elem->type = NUM;
	elem->bi = clean(str_to_bi(n));
	elem->prev = NULL;
	elem->next = NULL;
	return (elem);
}
