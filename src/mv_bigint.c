/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mv_bigint.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:37 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:07:40 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

void				shift_right(t_bigint *bi)
{
	long			i;

	i = 1;
	while (i < bi->len)
	{
		bi->val[i - 1] = bi->val[i];
		++i;
	}
	if (bi->len)
		bi->len--;
}
