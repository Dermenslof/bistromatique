/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   info.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:10 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/08 14:29:14 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info		g_info;

int					get_index(char c)
{
	size_t			i;

	i = 0;
	while (g_info.base[i] != c)
		++i;
	if (i >= g_info.base_len)
		print_error(2);
	return (i);
}

static char			*replace_all(char *expr)
{
	char			*t;
	char			*exp1;
	char			*exp2;

	exp1 = join(ft_strsub(g_info.opt, 2, 1), ft_strsub(g_info.opt, 0, 1));
	exp1 = join(exp1, ft_strsub(g_info.sep, 0, 1));
	exp2 = join(ft_strsub(g_info.opt, 2, 1), ft_strsub(g_info.opt, 1, 1));
	exp2 = join(exp2, ft_strsub(g_info.sep, 0, 1));
	while ((t = ft_strstr(expr, exp1)))
	{
		*(t + 2) = 0;
		expr = join(ft_strdup(expr), join(ft_strsub(g_info.base, 1, 1),
					ft_strsub(g_info.opt, 2, 1)));
		expr = join(expr, join(ft_strsub(g_info.sep, 0, 1), ft_strdup(t + 3)));
	}
	while ((t = ft_strstr(expr, exp2)))
	{
		*(t + 1) = 0;
		expr = join(ft_strdup(expr), ft_strdup(t + 2));
	}
	free(exp1);
	free(exp2);
	return (expr);
}

static char			*fix(char *expr)
{
	char			*tmp;
	char			*t;
	char			*ref[2];

	ref[0] = ft_strsub(g_info.base, 0, 1);
	ref[1] = join(ft_strsub(g_info.opt, 0, 1), ft_strsub(g_info.opt, 7, 1));
	tmp = expr;
	while (ft_isspace(*expr))
		expr++;
	expr = replace_all(expr);
	while ((t = ft_strstr(expr, ref[1])) && t != expr)
	{
		*(t + 1) = 0;
		expr = join(expr, join(ft_strsub(g_info.base, 1, 1),
					ft_strsub(g_info.opt, 2, 1)));
		expr = join(expr, join(ft_strsub(g_info.opt, 7, 1), ft_strdup(t + 2)));
	}
	if ((is_opt(*expr) && get_priority(*expr) == 1) && !is_num(expr[1]))
		expr = join(ft_strdup(ref[0]), ft_strdup(expr));
	free(ref[0]);
	free(ref[1]);
	if (tmp != expr)
		free(tmp);
	return (expr);
}

void				set_info(int ac, char **av)
{
	static t_f		*f[10] = {NULL};
	int				i;

	init_functions(f);
	g_info.ac = ac;
	g_info.cmd = *(av++);
	g_info.base = ft_strdup(*av);
	g_info.base_len = ft_strlen(*(av++));
	g_info.opt = ft_strdup(*(av++));
	g_info.sep = ft_strdup(*(av++));
	g_info.expr = fix(ft_strdup(*av));
	g_info.func = (t_func *)ft_memalloc(sizeof(t_func) * 9);
	g_info.stack = NULL;
	g_info.tmp = NULL;
	i = -1;
	while (f[++i])
	{
		g_info.func[i].opt = g_info.opt[i];
		g_info.func[i].f = f[i];
	}
	check_args();
}

void				set_default_info(int ac, char **av)
{
	static t_f		*f[10] = {NULL};
	int				i;

	init_functions(f);
	g_info.ac = ac;
	g_info.cmd = *(av++);
	g_info.base = ft_strdup("0123456789");
	g_info.base_len = 10;
	g_info.opt = ft_strdup("-+*/\%^!#&");
	g_info.sep = ft_strdup("()");
	g_info.expr = fix(ft_strdup(*av));
	g_info.func = (t_func *)ft_memalloc(sizeof(t_func) * 9);
	g_info.stack = NULL;
	g_info.tmp = NULL;
	i = -1;
	while (f[++i])
	{
		g_info.func[i].opt = g_info.opt[i];
		g_info.func[i].f = f[i];
	}
}
