/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:06:49 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:13:14 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

void					delete_infos(void)
{
	free(g_info.expr);
	free(g_info.base);
	free(g_info.opt);
	free(g_info.sep);
	free(g_info.func);
	if (g_info.stack)
		delete_elems(&g_info.stack);
	if (g_info.tmp)
		delete_elems(&g_info.tmp);
	if (g_info.tmp2)
		delete_elems(&g_info.tmp2);
}

void					delete_elems(t_elem **elem)
{
	if ((*elem)->next)
		delete_elems(&((*elem)->next));
	free_elem(elem);
}

void					print_usage(char *cmd)
{
	ft_putstr_fd("usage : ", 2);
	ft_putstr_fd(cmd, 2);
	ft_putstr_fd(USAGE, 2);
	exit(EXIT_SUCCESS);
}

void					print_error(int e)
{
	if (e == 1)
		ft_putstr_fd(SYNTAX_ERROR, 2);
	else if (e == 2)
		ft_putstr_fd(PARSE_ERROR, 2);
	else if (e == 3)
		ft_putstr_fd(MALLOC_ERROR, 2);
	else
		ft_putstr_fd(ZERO_ERROR, 2);
	delete_infos();
	exit(EXIT_SUCCESS);
}
