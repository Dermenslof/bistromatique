/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bigint.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:06:13 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:06:15 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info		g_info;

t_ulong				*new_ulong(long n)
{
	t_ulong			*ul;
	long			i;

	if (!(ul = (t_ulong *)malloc(sizeof(t_ulong) * n)))
		exit(3);
	i = 0;
	while (i < n)
		ul[i++] = 0;
	return (ul);
}

t_bigint			*new_bigint(long n)
{
	t_bigint		*bi;

	if (!(bi = (t_bigint *)malloc(sizeof(t_bigint))))
		exit(4);
	bi->val = new_ulong(n);
	bi->sign = 0;
	bi->len = n;
	return (bi);
}

t_bigint			*l_to_bi(long n)
{
	t_bigint		*bi;
	long			i;
	long			d;

	i = n;
	d = 0;
	while ((i /= g_info.base_len) > 0)
		++d;
	bi = new_bigint(d + 1);
	bi->len = !n ? 0 : d + 1;
	i = 0;
	while (n)
	{
		bi->val[i++] = n % g_info.base_len;
		n /= g_info.base_len;
	}
	return (bi);
}

t_bigint			*str_to_bi(char *str)
{
	t_bigint		*bi;
	long			i;

	i = ft_strlen(str);
	bi = new_bigint(i);
	i = 0;
	while (i < bi->len)
	{
		bi->val[i] = get_index(str[bi->len - i - 1]);
		++i;
	}
	return (bi);
}
