/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:57 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/08 15:35:10 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info		g_info;

void				print_bigint(t_bigint *bi)
{
	long			i;
	long			j;
	char			*str;

	str = malloc(bi->len + bi->len / 68 * 2 + 2);
	if (!bi->len && printf("0\n"))
		return ;
	if (bi->sign && bi->len)
		ft_putchar('-');
	i = 0;
	j = 0;
	while (i < bi->len)
	{
		str[i + j] = g_info.base[(int)bi->val[bi->len - i - 1]];
		++i;
		if (i % 68 == 0 && i != bi->len)
		{
			str[i + j++] = '\\';
			str[i + j++] = '\n';
		}
	}
	str[i + j] = 0;
	ft_putstr(str);
	free(str);
}

void				print_stack(t_elem *stack)
{
	long			count;

	count = 0;
	while (stack)
	{
		if (count++ > 0)
			ft_putchar(' ');
		ft_putchar('\'');
		if (stack->type == NUM)
		{
			if (stack->bi->sign)
				ft_putchar(*g_info.opt);
			print_bigint(stack->bi);
		}
		else
			ft_putchar(stack->c);
		ft_putchar('\'');
		stack = stack->next;
	}
	ft_putchar('\n');
}

void				print_result(t_elem **res)
{
	print_bigint((*res)->bi);
	ft_putchar('\n');
	free_elem(res);
}
