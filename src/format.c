/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   format.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:05 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:14:18 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info	g_info;

void			scale(t_ulong **ul, t_bigint *bi, long d)
{
	long		i;

	*ul = new_ulong(d);
	i = 0;
	while (i < d)
	{
		if (i < bi->len)
			(*ul)[i] = bi->val[i];
		else
			(*ul)[i] = 0;
		++i;
	}
}
