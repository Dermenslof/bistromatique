/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mv_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:42 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:34:54 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

void					push(t_elem **base, t_elem *elem)
{
	t_elem				*tmp;

	tmp = *base;
	elem->next = tmp;
	*base = elem;
}

void					push_end(t_elem **base, t_elem *elem)
{
	t_elem				*tmp;

	elem->next = NULL;
	if (!*base)
	{
		*base = elem;
		return ;
	}
	tmp = *base;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = elem;
	elem->prev = tmp;
	elem->next = NULL;
}

t_elem					*pop(t_elem **base)
{
	t_elem				*tmp;

	tmp = *base;
	if (tmp)
		*base = (*base)->next;
	return (tmp);
}

void					pop_middle(t_elem **expr, t_elem *elem)
{
	t_elem				*tmp;

	tmp = elem->prev;
	elem->prev = NULL;
	if (tmp->prev)
	{
		elem->prev = tmp->prev;
		tmp->prev->next = elem;
	}
	else
		*expr = elem;
	free_elem(&tmp);
}

void					pop_expr(t_elem **base)
{
	t_elem				*tmp;
	t_elem				*prev;

	tmp = *base;
	prev = NULL;
	while (tmp->next)
	{
		prev = tmp;
		tmp = tmp->next;
	}
	if (!prev)
		*base = NULL;
	else
		prev->next = NULL;
	free_elem(&tmp);
}
