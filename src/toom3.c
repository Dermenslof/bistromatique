
#include "bistro.h"

extern t_info	g_info;

void				*toom3(t_elem **num, t_elem *dec)
{
	t_elem		*p[5];
	t_elem		*ta[5];
	t_elem		*tb[5];

	p[0] = pre_kara(cpy(num[0]), cpy(num[3]));
	p[4] = pre_kara(cpy(num[2]), cpy(num[5]));
	ta[0] = pre_add(cpy(num[0]), cpy(num[2]));
	tb[0] = pre_add(cpy(num[3]), cpy(num[5]));
	ta[2] = pre_add(cpy(ta[0]), cpy(num[1]));
	tb[2] = pre_add(cpy(tb[0]), cpy(num[4]));
	ta[3] = pre_sub(ta[0], num[1]);
	tb[3] = pre_sub(tb[0], num[4]);
	ta[4] = pre_add(cpy(ta[3]), num[2]);
	ta[4] = pre_kara(ta[4], new_num(l_to_bi(2)));
	ta[4] = pre_sub(ta[4], num[0]);
	tb[4] = pre_add(cpy(tb[3]), num[5]);
	tb[4] = pre_kara(tb[4], new_num(l_to_bi(2)));
	tb[4] = pre_sub(tb[4], num[3]);
	p[1] = pre_kara(ta[2], tb[2]);
	p[2] = pre_kara(ta[3], tb[3]);
	p[3] = pre_kara(ta[4], tb[4]);
	p[3] = pre_div(pre_sub(p[3], cpy(p[1])), new_num(l_to_bi(3)));
	p[1] = pre_div(pre_sub(p[1], cpy(p[2])), new_num(l_to_bi(2)));
	p[2] = pre_sub(p[2], cpy(p[0]));
	p[3] = pre_div(pre_sub(cpy(p[2]), p[3]), new_num(l_to_bi(2)));
	p[3] = pre_add(p[3], pre_kara(cpy(p[4]), new_num(l_to_bi(2))));
	p[2] = pre_sub(pre_add(p[2], cpy(p[1])), cpy(p[4]));
	p[1] = pre_sub(p[1], cpy(p[3]));
	ta[0] = cpy(dec);
	p[0] = pre_add(p[0], pre_kara(p[1], cpy(dec)));
	dec = pre_kara(dec, cpy(ta[0]));
	p[0] = pre_add(p[0], pre_kara(p[2], cpy(dec)));
	dec = pre_kara(dec, cpy(ta[0]));
	p[0] = pre_add(p[0], pre_kara(p[3], cpy(dec)));
	dec = pre_kara(dec, ta[0]);
	p[0] = pre_add(p[0], pre_kara(p[4], cpy(dec)));
	return (p[0]);
}

t_elem			*pre_toom3(t_elem *a, t_elem *b)
{
	t_elem		*xy[6];
	t_elem		*dec;
	size_t		d;

	if (a->bi->len < 100 || b->bi->len < 100)
		return (pre_kara(a, b));
	d = (b->bi->len / 5) * 2;
	xy[0] = cpy_l(a->bi->val, d);
	xy[1] = cpy_l(&a->bi->val[d], d);
	xy[2] = cpy_l(&a->bi->val[2 * d], a->bi->len - 2 * d);
	xy[3] = cpy_l(b->bi->val, d);
	xy[4] = cpy_l(&b->bi->val[d], d);
	xy[5] = cpy_l(&b->bi->val[2 * d], b->bi->len - 2 * d);
	dec = new_num(new_bigint(d));
	dec->bi->val[d] = 1;
	xy[0] = toom3(xy, dec);
	free_elem(&a);
	free_elem(&b);
	return (xy[0]);
}
