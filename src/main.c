/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:07:18 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:33:23 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

t_info				g_info;

int					main(int ac, char **av)
{
	if (ac == 5)
		set_info(ac, av);
	else if (ac == 2)
		set_default_info(ac, av);
	else
		print_usage(*av);
	parse(&g_info.stack, g_info.expr);
	eval(&g_info.stack);
	delete_infos();
	return (EXIT_SUCCESS);
}
