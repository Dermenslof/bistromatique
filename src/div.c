/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   div.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:06:22 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/06 17:41:26 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bistro.h"

extern t_info			g_info;

static char				free_elems(t_elem *a, t_elem *b, t_elem *z, t_elem *tmp)
{
	free_elem(&a);
	free_elem(&b);
	free_elem(&z);
	free_elem(&tmp);
	return (1);
}

t_elem					*div_by_two(t_elem *a)
{
	t_elem				*r;
	long				i;
	long				id;
	long				carry;

	r = new_num(new_bigint(a->bi->len));
	i = a->bi->len;
	r->bi->len = i;
	carry = 0;
	while (i--)
	{
		id = a->bi->val[i] + carry;
		r->bi->val[i] = r->bi->val[i] + id / 2;
		if (i == a->bi->len - 1 && r->bi->val[i] == 0)
		{
			r->bi->len--;
			r->bi->val[i] = 0;
		}
		carry = i && id % 2 && g_info.base_len % 2;
		if (i && id % 2)
			r->bi->val[i - 1] = g_info.base_len / 2;
	}
	free_elem(&a);
	return (!r->bi->len && free_elem(&r) ? new_num(l_to_bi(0)) : r);
}

static t_elem			*div_by_digit(t_elem *a, t_elem *b, long i)
{
	t_ulong				d;
	t_ulong				num;
	t_elem				*r;

	r = new_num(new_bigint(a->bi->len));
	d = b->bi->val[0];
	while (i < a->bi->len)
	{
		num = a->bi->val[a->bi->len - i - 1];
		if (i)
			num += a->bi->val[a->bi->len - i] * g_info.base_len;
		++i;
		if (num < d)
		{
			r->bi->val[a->bi->len - i] = 0;
			continue ;
		}
		r->bi->val[a->bi->len - i] = num / d;
		a->bi->val[a->bi->len - i] = num % d;
	}
	free_elem(&a);
	free_elem(&b);
	clean(r->bi);
	return (r);
}

static t_elem			*div_by_elem(t_elem *a, t_elem *b, long i)
{
	t_elem				*r;
	t_elem				*tmp;
	char				d;
	t_elem				*ref;

	ref = new_num(new_bigint(i));
	ref->bi->val[i - 1] = 1;
	r = cpy(ref);
	while (i--)
	{
		while (cmp((tmp = pre_mul(cpy(b), cpy(r)))->bi, a->bi) < 0
				&& free_elem(&tmp))
			r = pre_add(r, cpy(ref));
		ref->bi->len -= 1;
		if ((d = !cmp(tmp->bi, a->bi)) || !ref->bi->len)
			break ;
		ref->bi->val[i - 1] = 1;
		free_elem(&tmp);
		while (cmp((tmp = pre_mul(cpy(b), cpy(r)))->bi, a->bi) > 0
				&& free_elem(&tmp))
			r = pre_sub(r, cpy(ref));
		free_elem(&tmp);
	}
	r = free_elems(a, b, tmp, ref) && !d ? pre_sub(r, new_num(l_to_bi(1))) : r;
	return (r);
}

t_elem					*pre_div(t_elem *a, t_elem *b)
{
	if (a->bi->sign && b->bi->sign)
		return (pre_div(inv(a), inv(b)));
	else if (a->bi->sign)
		return (inv(pre_div(inv(a), b)));
	else if (b->bi->sign)
		return (inv(pre_div(a, inv(b))));
	else if (!b->bi->len)
	{
		free_elems(a, b, NULL, NULL);
		print_error(4);
	}
	else if (!a->bi->len && free_elems(a, b, NULL, NULL))
		return (new_num(l_to_bi(0)));
	if (b->bi->len == 1)
		return (div_by_digit(a, b, 0));
	return (div_by_elem(a, b, a->bi->len - b->bi->len + 1));
}
