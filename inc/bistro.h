/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bistro.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 17:05:58 by fmontaro          #+#    #+#             */
/*   Updated: 2015/01/09 15:06:46 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BISTRO_H

# define BISTRO_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <libft.h>
# include <stdint.h>

# define ABS(X)			((X) < 0 ? -(X) : (X))
# define MAX(X, Y)		((X) > (Y) ? (X) : (Y))
# define MIN(X, Y)		((X) < (Y) ? (X) : (Y))

# define USAGE			"[<base> <operators> <separators>] <expression>\n"
# define SYNTAX_ERROR	"syntax error\n"
# define PARSE_ERROR	"parse_error\n"
# define MALLOC_ERROR	"malloc error\n"
# define ZERO_ERROR		"divide by zero\n"

# define SEP			0
# define OPT			1
# define NUM			2

# define KARA_CUTOFF	32

typedef uint32_t		t_ulong;
typedef struct s_bigint	t_bigint;
typedef struct s_kara	t_kara;
typedef struct s_elem	t_elem;
typedef struct s_info	t_info;
typedef struct s_func	t_func;
typedef t_elem*(t_f)(t_elem *, t_elem *);

struct					s_bigint
{
	t_ulong				*val;
	long				len;
	char				sign;
};

struct					s_func
{
	char				opt;
	t_elem				*(*f)(t_elem *, t_elem *);
};

struct					s_info
{
	int					ac;
	char				*cmd;
	char				*base;
	size_t				base_len;
	char				*opt;
	char				*sep;
	char				*expr;
	t_func				*func;
	t_elem				*stack;
	t_elem				*tmp;
	t_elem				*tmp2;
};

struct					s_elem
{
	char				type;
	t_bigint			*bi;
	char				c;
	char				do_pop;
	t_elem				*prev;
	t_elem				*next;
};

struct					s_kara
{
	t_ulong				*r[2];
	t_ulong				*l[2];
	t_ulong				*sum[2];
	t_ulong				*x1;
	t_ulong				*x2;
	t_ulong				*x3;
};

void					init_functions(t_f **f);
void					set_info(int ac, char **av);
void					set_default_info(int ac, char **av);
void					check_args(void);

void					delete_infos(void);
void					delete_elems(t_elem **elem);
void					print_usage(char *cmd);
void					print_error(int e);

t_elem					*new_num(t_bigint *bi);
t_elem					*new_nb(char *n);
t_elem					*new_opt(char c);
t_elem					*new_sep(char c);

void					push(t_elem **base, t_elem *elem);
void					push_end(t_elem **base, t_elem *elem);
t_elem					*pop(t_elem **base);
void					pop_expr(t_elem **base);
void					pop_middle(t_elem **expr, t_elem *elem);
char					free_elem(t_elem **elem);

void					set_sep(t_elem *elem);
void					set_opt(t_elem **expr, t_elem *elem);
void					set_nb(t_elem **expr, t_elem *elem);
void					parse(t_elem **stack, char *str);
void					shuting_yard(t_elem **stack, t_elem **expr);
void					eval(t_elem **stack);

char					is_opt(char c);
char					is_sep(char c);
char					is_num(char c);
char					get_priority(char c);
int						get_index(char c);
char					*join(char *a, char *b);

char					is_odd(t_bigint *bi);
t_elem					*inv(t_elem *n);
char					cmp(t_bigint *a, t_bigint *b);
t_bigint				*new_bigint(long n);
t_ulong					*new_ulong(long n);
t_bigint				*l_to_bi(long n);
t_bigint				*str_to_bi(char *str);
void					scale(t_ulong **ul, t_bigint *bi, long d);
t_elem					*cpy(t_elem *n);
t_elem					*cpy_l(t_ulong *n, long d);
t_elem					*div_by_two(t_elem *a);
t_bigint				*clean(t_bigint *bi);

void					print_stack(t_elem *stack);
void					print_result(t_elem **res);
void					print_bigint(t_bigint *bi);

t_elem					*pre_add(t_elem *a, t_elem *b);
t_elem					*pre_sub(t_elem *a, t_elem *b);
t_elem					*pre_mul(t_elem *a, t_elem *b);
t_elem					*pre_kara(t_elem *a, t_elem *b);
t_elem					*pre_toom3(t_elem *a, t_elem *b);
t_elem					*pre_div(t_elem *a, t_elem *b);
t_elem					*pre_mod(t_elem *a, t_elem *b);
t_elem					*pre_pow(t_elem *a, t_elem *b);
t_elem					*pre_fac(t_elem *a, t_elem *unused);
t_elem					*pre_max(t_elem *a, t_elem *b);
t_elem					*pre_min(t_elem *a, t_elem *b);

#endif
