#!/bin/zsh

OK="\033[32m"
BAD="\033[31m"

if [ -f "$1" ]; then
	echo -n ""
else
	echo -n "$BAD"
	echo "error"
	echo -n "\033[0m"
	exit
fi

if [ -f "$2" ]; then
	echo -n ""
else
	echo -n "$BAD"
	echo "error"
	echo -n "\033[0m"
	exit
fi

A=$(cat $1)
B=$(cat $2)
if [ "$A" = "$B" ]; then
	echo -n "$OK"
	echo "ok"
else
	echo -n "$BAD"
	echo  "error"
fi
echo -n "\033[0m"
